import turtle
import time
import random


# Turtle design
def new(turtle, shape, color, x, y):
    turtle.speed(0)
    turtle.shape(shape)
    turtle.color(color)
    turtle.penup()
    turtle.goto(x, y)


delay = 0.1
# score
score = 0
high_score = 0
# set up the screen
game = turtle.Screen()
game.title("snake game(itay and matan)")
game.bgcolor("black")
game.setup(width=600, height=600)
game.tracer(0)  # turn off screen updates
# snake head
head = turtle.Turtle()
new(head, "square", "blue", 0, 0)
head.direction = "stop"
# snake food
food = turtle.Turtle()
new(food,"circle", "red", 0, 100)
snake_body = []

# pen
pen = turtle.Turtle()
pen.hideturtle()
new(pen, "square", "white", 0, 260)
pen.write("score: 0 high score: 0", align="center", font=('david', 25, 'normal'))


# functions
def go_up():
    if head.direction != "down":
        head.direction = "up"


def go_down():
    if head.direction != "up":
        head.direction = "down"


def go_left():
    if head.direction != "right":
        head.direction = "left"


def go_right():
    if head.direction != "left":
        head.direction = "right"


def move():
    if head.direction == "up":
        y = head.ycor()
        head.sety(y + 20)
    if head.direction == "down":
        y = head.ycor()
        head.sety(y - 20)
    if head.direction == "left":
        x = head.xcor()
        head.setx(x - 20)
    if head.direction == "right":
        x = head.xcor()
        head.setx(x + 20)


def restart():
    global snake_body, part, score, delay
    time.sleep(1)
    head.goto(0, 0)
    head.direction = 'stop'
    # hide the snake_body
    for part in snake_body:
        part.goto(1000, 1000)
    # clear the snake_body list
    snake_body = []
    # reset score
    score = 0
    # reset delay
    delay = 0.1
    #
    pen1()


def pen1():
    pen.clear()
    pen.write('score: {} high_score: {}'.format(score, high_score), align='center', font=('david', 24, 'normal'))


# keyboard bindings
game.listen()
game.onkeypress(go_up, 'w')
game.onkeypress(go_down, 's')
game.onkeypress(go_left, 'a')
game.onkeypress(go_right, 'd')
# main game loop
while True:
    game.update()
    # Check if the snake touches the border
    if head.xcor() > 290 or head.xcor() < -290 or head.ycor() > 290 or head.ycor() < -290:
        restart()

    # Check if the snake touches the food
    if head.distance(food) < 20:
        # move the food to a random spot
        x = random.randint(-290, 290)
        y = random.randint(-290, 290)
        food.goto(x, y)
        # add a part
        new_part = turtle.Turtle()
        new(new_part, "square", "green", 1000, 1000)
        snake_body.append(new_part)
        # increase the score
        score += 10
        if score > high_score:
            high_score = score
        pen1()

    # move the end snake_body in revers order follow each other
    for i in range(len(snake_body) - 1, 0, -1):
        x = snake_body[i - 1].xcor()
        y = snake_body[i - 1].ycor()
        snake_body[i].goto(x, y)
    # move part 0 to where is the head
    if len(snake_body) > 0:
        x = head.xcor()
        y = head.ycor()
        snake_body[0].goto(x, y)
    move()

    # Check if the snake touches itself
    for part in snake_body:
        if part.distance(head) < 20:
            restart()
    time.sleep(delay)
